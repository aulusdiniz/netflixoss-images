# MSDAF Cloud images

Centralize the sources needed to build and publish our cloud runtime applications.

## Current Services Available

### Discovery

The first application to be ran is the discovery of micro services. Each new service connects 
to the discovery and provide its interfaces of communication. The discovery then, presents this data into
a requestable format so that other apps can communicate with each other.

We are using Eureka as our default Discovery Service.

### Config Server

The config server is responsible for providing centralized and unified config properties for all services.
In our ecossystem, the Config server is provided as a micro service registered in Discovery.

## How to Setup

### Building local images

To config the environment you have to build the spring-config-server and discovery-service, as shown:

`bash
  ./gradlew clean buildDocker
`

### Using DockerHub images

If you want to use romote images in DockerHub, or after building the local image, you can run the docker compose provided:

`bash
  docker-compose up -d
`

After a couple of minutes both services must be running and the config-server should be registered as a service in Eureka.
Try to access both services:
 - *Config Server:* http://localhost:8888/foo/development 
 - *Discovery Service:* http://localhost:8761/ 

If you are using Docker Toolbox or docker-machine, consider running `docker-machine ls` to list the docker main machine used
and then `docker-machine ip <MACHINE>` to get te ip to access the services.
