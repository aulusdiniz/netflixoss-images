# Run spring-cloud-config
1. Clone o código do Git abaixo
`git clone git@github.com:hyness/spring-cloud-config-server.git`

2. Na pasta criada “spring-cloud-config-server”

3. Crie a imagem no docker com o seguindo código: (inclua o nome da imagem no lugar de *nome_da_imagem*)
`docker build -t nome_da_imagem .``
> OBS.: vai levar um tempo para baixar o que ele precisa para rodar  

4. Para rodar a imagem insira o código abaixo no terminal
`docker run -it -p 8888:8888 -e SPRING_CLOUD_CONFIG_SERVER_GIT_URI=https://github.com/spring-cloud-samples/config-repo nome_da_imagem`
> OBS.: esse código puxa as configurações do GitHub.  

5. Basta acessar a url: http://localhost:8888 para testar

# Subindo a imagem para o docker hub
1. Com a imagem ja criada digite o seguinte código no terminal
`docker tag nome_da_imagem id_docker_hub/nome_da_imagem_hub:tag_version`

2. Insira o seguinte código para dar um push para o DockeHub e aguarde
`docker push id_docker_hub/nome_da_imagem_hub:tag_version`

Fontes
[Docker Hub - kcomlabs](https://hub.docker.com/r/kcomlabs/spring-cloud-config-server)
[Docker Hub - hyness](https://hub.docker.com/r/hyness/spring-cloud-config-server/)
[GitHub - hyness/spring-cloud-config-server: Docker build of the spring-cloud-config-server](https://github.com/hyness/spring-cloud-config-server)
[Spring Cloud Config - Documentation](https://cloud.spring.io/spring-cloud-static/spring-cloud-config/2.1.1.RELEASE/single/spring-cloud-config.html)
[Executando aplicações Spring Boot no Docker | Emmanuel Neri](https://emmanuelneri.com.br/2017/09/04/executando-aplicacoes-spring-boot-com-docker/)
[Reconfiguring Applications with Spring Cloud Config Server - DZone Cloud](https://dzone.com/articles/spring-cloud-config-server-for-the-impatient)
